# Delete Gitlab spam users

## Installation

```
sudo cpanm Carton
git clone https://framagit.org/framasoft/gitlab/delete-gitlab-spammers.git
cd delete-gitlab-spammers
carton install --deployment
cp config.json.template config.json
vi config.json
carton exec ./delete-gitlab-spammers.pl
```

## LICENSE

© 2019 Framasoft, GPLv3
See the [LICENSE file](LICENSE).
